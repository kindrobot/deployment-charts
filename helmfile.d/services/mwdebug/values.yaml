docker:
  registry: docker-registry.discovery.wmnet
main_app:
  requests:
    cpu: 4000m
    memory: 1000Mi
  limits:
    cpu: 5000m
    memory: 2800Mi
resources:
  replicas: 2

service:
  deployment: production
  expose_http: true
  port:
    nodePort: 8444

tls:
  enabled: true
  public_port: 4444
  resources:
    requests:
      cpu: 200m
      memory: 100Mi
    limits:
      cpu: 750m
      memory: 350Mi
monitoring:
  enabled: true

debug:
  enabled: false
  ports: []
  php:
    enabled: true

# See the dockerfiles for the php-fpm base image to know what can be tweaked.
# Those can be passed via config.public above.
php:
  slowlog_timeout: 1
  workers: 8
  servergroup: kube-mwdebug
  fcgi_mode: FCGI_UNIX
  exporter_version: 0.0.2
  httpd:
    exporter_version: 0.0.3
    requests:
      cpu: 200m
      memory: 200Mi
    limits:
      cpu: 500m
      memory: 400Mi
  opcache:
    nofiles: "32531"
    size: "500"
    interned_strings_buffer: "50"
  apc:
    size: "768M"
mw:
  domain_suffix: "org"
  egress:
    etcd_servers:
      # conf1007
      - ip: 10.64.0.207
        port: 4001
      - ip: 2620:0:861:101:10:64:0:207
        port: 4001
      # conf1008
      - ip: 10.64.16.110
        port: 4001
      - ip: 2620:0:861:102:10:64:16:110
        port: 4001
      # conf1009
      - ip: 10.64.48.154
        port: 4001
      - ip: 2620:0:861:107:10:64:48:154
        port: 4001
      # conf2004
      - ip: 10.192.16.45
        port: 4001
      - ip: 2620:0:860:102:10:192:16:45
        port: 4001
      # conf2005
      - ip: 10.192.32.52
        port: 4001
      - ip: 2620:0:860:103:10:192:32:52
        port: 4001
      # conf2006
      - ip: 10.192.48.59
        port: 4001
      - ip: 2620:0:860:104:10:192:48:59
        port: 4001
  httpd:
    image_tag: "restricted/mediawiki-webserver:2021-07-26-084018-webserver"
    additional_config: |-
      LoadModule remoteip_module /usr/lib/apache2/modules/mod_remoteip.so
      RemoteIPHeader X-Client-IP
      RemoteIPInternalProxy 10.0.0.0/8
      RemoteIPInternalProxy 127.0.0.1/32
  mcrouter:
    enabled: true
    image_tag: mcrouter:0.41.0-4-20210718
    exporter_version: 0.0.1-2
    resources:
      requests:
        cpu: 200m
        memory: 100Mi
      limits:
        cpu: 700m
        memory: 200Mi
  logging:
    resources:
      requests:
        cpu: 100m
        memory: 200m
# network egress for various services
networkpolicy:
  egress:
    enabled: true
    dst_nets:
      # swift is covered by envoy even if still not used in mediawiki.
      # url-downloader is covered by the default egress rules
      # redis_lock is covered by the nutcracker-originated rules for redis
      # Common defs
      #IRC - TODO: define the port range once we upgrade k8s
      # irc1001
      - cidr: 208.80.155.105/32
      # irc2001
      - cidr: 208.80.153.62/32
      # xenon
      # mwlog1002
      - cidr: 10.64.32.141/32
        ports:
          - protocol: TCP
            port: 6379
      # statsd
      # graphite1004 / statsd.eqiad.wmnet
      - cidr: 10.64.16.149/32
        ports:
          - protocol: UDP
            port: 8125
      # graphite2003 / statsd.eqiad.wmnet
      - cidr: 10.192.0.102/32
        ports:
          - protocol: UDP
            port: 8125
      # eventlogging
      # eventlog1001
      - cidr: 10.64.32.167/32
        ports:
          - protocol: TCP
            port: 8421
      # logstash
      # logstash.svc.eqiad.wmnet
      - cidr: 10.2.2.36/32
      # DC-specific defs
      ## Eqiad
      # Poolcounter
      # poolcounter1004
      - cidr: 10.64.0.151/32
        ports:
          - protocol: TCP
            port: 7531
      # poolcounter1005
      - cidr: 10.64.32.236/32
        ports:
          - protocol: TCP
            port: 7531
      # udp2log
      # mwlog1002
      - cidr: 10.64.32.141/32
        ports:
          - protocol: UDP
            port: 8420

      ## Codfw
      # Poolcounter
      # poolcounter2003
      - cidr: 10.192.0.132/32
        ports:
          - protocol: TCP
            port: 7531
      # poolcounter2004
      - cidr: 10.192.16.129/32
        ports:
          - protocol: TCP
            port: 7531
      # udp2log
      # mwlog2002
      - cidr: 10.192.32.9/32
        ports:
          - protocol: UDP
            port: 8420

# Only deploy to nodes without spinning disks T288345
affinity:
  nodeSelector:
    node.kubernetes.io/disk-type: ssd
kafka:
  allowed_clusters:
    - logging-eqiad
    - logging-codfw
